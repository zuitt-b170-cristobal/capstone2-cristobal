//dependencies
const User = require("../models/user.js")
const Product = require("../models/product.js")
const Order = require("../models/order.js")
const auth = require("../auth.js")
const bcrypt = require("bcrypt")//used to encrypt user password


// retrieve all products
module.exports.getAllProducts = () => {
	return Product.find( {} ).then((result, error) => {
		if (error) {
			return false
		}else{
			return result
		}
	})
}

//Create a Product (Admin)
module.exports.addProduct = (reqBody, userData) => {

    return User.findById(userData.userId).then(result => {

        if (userData.isAdmin === false) {
            return "You are not an admin"
        } else {
            let newProduct = new Product({
                name: reqBody.name,
                description: reqBody.description,
                price: reqBody.price
            })
        
            //Saves the created object to the database
            return newProduct.save().then((product, error) => {
                //Product creation failed
                if(error) {
                    return false
                } else {
                    //product creation successful
                    return "Product creation successful"
                }
            })
        }
        
    });    
}

//Retrieve a Single Product
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams).then(result => {
		return result
	})
}

// Update Product Info (Admin)
module.exports.updateProduct = ( reqParams, reqBody ) => {
	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}
	// findByIdAndUpdate - looks for the id of the document (first parameter) and updates it (content of the second parameter)
		//the server will be looking for the id of the reqParams.courseId in the database and updates the document through the content of the object updatedCourse 
	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((result, error) => {
		if (error) {
			return false
		} else	{
			return true
		}
	})
}

//Archive a Product (Admin)
module.exports.archivedProduct = (reqParams, reqBody) => {
		let updatedProduct = {
			isActive: reqBody.isActive
	}
	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((result, error) => {
		if (error) {
			return false
		} else	{
			return true
		}
	})
}
