//dependencies
const User = require("../models/user.js")
const Product = require("../models/product.js")
const Order = require("../models/order.js")
const auth = require("../auth.js")
const bcrypt = require("bcrypt")//used to encrypt user password


//Checkout (NON-Admin only)
module.exports.orderItem = (reqBody, userData) => {

    return User.findById(userData.userId).then(result => {

        if (userData.isAdmin === true) {
            return "You cannot proceed to checkout"
        } else {
            let newOrder = new Order({
                totalAmount: reqBody.totalAmount,
                userId: reqBody.userId,
                productsPurchased: reqBody.productsPurchased   
            })
            //Saves the created object to the database
            return newOrder.save().then((course, error) => {
                //Order creation failed
                if(error) {
                    return false
                } else {
                    //order creation successful
                    return "Order creation successful"
                }
            })
        }
        
    });    
}
/*module.exports.checkoutItem = async (data, requestBody) => {
	//finding user in database
	let isUserUpdated = await User.findById(data.userId).then(user => {
		if (data.isAdmin === true) {
            return "You cannot proceed to checkout"
        } else {
        //adding the productId to the user's orders array
		user.orders.push({productId: data.productId})
		//saving in database
		return user.save().then((user, err) => {
			if (err){
				return false
			}else{
				return true
			}
		})
	}
})*/
  /*  let isOrderUpdated = await Product.findById(data.productId).then(product => {
    	order.processedOrders.push({userId: data.userId})
    	//saving in database
		return order.save().then((order, err) => {
			if (err){
				return false
			}else{
				return true
			}
		})
	}) 
    if (isUserUpdated, isOrderUpdated){
		return true
	}
	else{
		return false
	}
}
*/




//Retrieve Authenticated User's Order (Admin Only)
module.exports.userOrders = (requestBody) => {
	return User.find({ email: requestBody.email }).then((result, error) => {
		if (error) {
			console.log(error)
			return false
		}else{
			if (result.length > 0) {
				//return result
				return result
			}else{
				//return res.send("email does not exist")
				return false
			}
		}
	})
}

// Retrieve All Orders (Admin only*)
module.exports.getAllOrders = () => {
	return Order.find( {} ).then((result, error) => {
		if (error) {
			return false
		}else{
			return result
		}
	})
}


