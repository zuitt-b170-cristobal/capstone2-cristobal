//dependencies
const User = require("../models/user.js")
const Product = require("../models/product.js")
const Order = require("../models/order.js")
const auth = require("../auth.js")
const bcrypt = require("bcrypt")//used to encrypt user password


//Check if Email Exists
module.exports.checkEmail = (requestBody) => {
	return User.find({ email: requestBody.email }).then((result, error) => {
		if (error) {
			console.log(error)
			return false
		}else{
			if (result.length > 0) {
				//return result
				return true
			}else{
				//return res.send("email does not exist")
				return false
			}
		}
	})
}


//User Registration
module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		email: reqBody.email,
		//hashSync is a function of bcrypt that encrypts the password
			//10 is the # of times it runs the algorithm to the reqBody.password; max 72 implementations for security measures
		password: bcrypt.hashSync(reqBody.password, 10),
	})
	return newUser.save().then((saved, error) => {
		if (error){
			console.log(error)
			return false
		}else{
			return true
		}
	})
}

//User Authentication/Login
module.exports.userLogin = (reqBody) => {
	return User.findOne( { email: reqBody.email } ).then(result => {
		if (result === null) {
			return false
		} else {
			// compareSync function - used to compare a non-encrypted password to an encrypted password and retuns a Boolean reasponse depending on the result
			/*
			What should we do after the comparison?
				true - a token should be created since the user is existing and the password is correct
false - the passwords do not match, thus a toke should not be created
			*/
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if (isPasswordCorrect) {
				return {access: auth.createAccessToken(result.toObject())}
				// auth - imported auth.js
				// createAccessToken - function inside the auth.js to create access token
				// .toObject - transforms the User into an Object that can be used by our createAccessToken
			} else {
				return false
			}
		}
	})
}

//Get User Profile
module.exports.getProfile = (data) => {
	return User.findById(data.userId).then(result => {
		if (result === null) {
			return false
		} else {
			result.password = "*****"; //make password invisible
			return result
		}
	})
}

//Set User As Admin(Admin only)
module.exports.setAdminUser = (reqParams, reqBody) => {
	let setAsAdmin = {
	isAdmin: reqBody.isAdmin
	}
	return User.findByIdAndUpdate(reqParams.userId, setAsAdmin).then((result, error) => {
		if (error) {
			return false
		}else{
			return true
		}
	}) 
}













