const express = require("express")
const router = express.Router()

const auth = require("../auth.js")
const userController = require("../controllers/userController.js")
const productController = require("../controllers/productController")
const orderController = require("../controllers/orderController.js")


//checking email
router.post("/checkEmail", (req, res)=> {
	userController.checkEmail(req.body).then(resultFromController => res.send(resultFromController))
})


//User Registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
})

//User Authentication/Login
router.post("/login", (req, res) => {
	userController.userLogin(req.body).then(resultFromController => res.send(resultFromController))
})


//Get User Profile
//auth.verify - ensures that a user is logged in before proceeding to the next part of the code; this is the verify function inside the auth.js
router.get("/details", auth.verify, (req, res) => {
	//decode - decrypts token inside the authorization (w/c is in headers of request)
	//req.headers.authorization contains the token that was creeated for the user
	const userData = auth.decode(req.headers.authorization)
	userController.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController))	
})


//Set User As Admin(Admin only)
router.put("/:userId/setAdmin", auth.verify, (req, res) => {
	userController.setAdminUser(req.params, req.body).then(result => res.send(result))
})



module.exports = router