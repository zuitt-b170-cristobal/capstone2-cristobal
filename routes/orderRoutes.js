const express = require("express")
const router = express.Router()

const auth = require("../auth.js")
const productController = require("../controllers/productController")
const userController = require("../controllers/userController.js")
const orderController = require("../controllers/orderController.js")




//Checkout (NON-Admin only)
router.post("/users/addOrder", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization) //userData would now contain an object that has the token payload (id, email, isAdmin information of the user)
    orderController.orderItem(req.body, userData).then(resultFromController => res.send(resultFromController))
})


/*router.post("/users/checkout", auth.verify, (req, res) => {
	//decode - decrypts token inside the authorization (w/c is in headers of request)
	//req.headers.authorization contains the token that was created for the user
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		//in the decoded token, the only thing we'll be looking at/storing is the .id
		productId: req.body.productId
	}
	orderController.checkoutItem(data).then(result => res.send(result))	
})
*/


//Retrieve Authenticated User's Order (Admin Only)
router.get("/users/myOrders", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization) //userData would now contain an object that has the token payload (id, email, isAdmin information of the user)
    orderController.userOrders(req.body, userData).then(resultFromController => res.send(resultFromController))
})






// Retrieve All Orders
router.get("/users/orders", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	orderController.getAllOrders().then(resultFromController => res.send(resultFromController))
})




module.exports = router