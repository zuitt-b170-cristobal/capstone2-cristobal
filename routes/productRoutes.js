const express = require("express")
const router = express.Router()

const auth = require("../auth.js")
const productController = require("../controllers/productController")
const userController = require("../controllers/userController.js")
const orderController = require("../controllers/orderController.js")

// Retrieve All Products
router.get("/", (req, res) => {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController))
})

//Create a Product (Admin)
router.post("/", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization) //userData would now contain an object that has the token payload (id, email, isAdmin information of the user)
    productController.addProduct(req.body, userData).then(resultFromController => res.send(resultFromController))
})

// Retrieve a Product
router.get("/:productId",  (req, res) => {
	console.log(req.params.productId);
	productController.getProduct(req.params.productId).then(result => res.send(result))
})

// Update Product Info (Admin)
router.put("/:productId", auth.verify, (req, res) => {
	productController.updateProduct(req.params, req.body).then(result => res.send(result))
})

//Archive a Product (Admin)
router.put("/:productId/archive", auth.verify, (req, res) => {
	productController.archivedProduct(req.params, req.body).then(result => res.send(result))
})






module.exports = router