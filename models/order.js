const mongoose = require("mongoose")

const orderSchema = new mongoose.Schema({
	totalAmount:{
		type: Number,
		required: [true, "Total amount is required"]
	},
	purchasedOn:{
		type: Date,
		default: new Date()
	},
	userId:{
		type: String, 
		required: [true, "User ID is required"]
		},
	productsPurchased:[
			{
			productId:{
			type: String,
			required: [true, "Product id is required"]		
			},
			name:{
			type: String,
			required: [true, "Product name is required"]
			}
		}
	]
})







module.exports = mongoose.model("Order", orderSchema);
