const mongoose = require("mongoose")

const userSchema = new mongoose.Schema({
	email:{ 
		type: String,
		required: [true, "First name is required"]
	},
	password:{
		type: String,
		required: [true, "Password is required"]
	},
	isAdmin:{ 
		type: Boolean,
		default: false
	},
	orders:[
	{
		productId:{ 
			type: String,
			required: [true, "Product id is required"]
		},
		orderedOn:{
			type: Date,
			default: new Date()
		}
	}
	]

})





module.exports = mongoose.model("User", userSchema);
