const jwt = require("jsonwebtoken")
const secret = "IDoNotKnow" //form of message/string in w/c this will serve as our secret code

//JSON WebToken - way of securely passing info from a part of server to the front end
//info is kept secure through the use of secret variable 
//the secret variable is only known by the system/browser w/c can be decoded by the server

//token creation
module.exports.createAccessToken = (user) => {
	//data will be received from the registration form, when a user logs in, a token will be created w the user's info (this info still encrypted inside the token) 
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}
	//generates token using the form data & secret code w/o add'l options
	return jwt.sign(data, secret, {})
}

//token verification - 
module.exports.verify = (req, res, next) => { //next parameter - tells server to proceed if verification is OK/successful
	let token = req.headers.authorization //authorization can be found in the headers of the request & tells whether the client/user has the authority to send the request
	if (typeof token !== "undefined") { //token is present
		console.log(token)
		token = token.slice(7, token.length)
		//jwt.verify - verifies token using secret, & fails if token's secret does not match the secret var meaning there is attempt to hack, or tamper/change data from the user-end
		return jwt.verify(token, secret, (err, data) => {
			if(err){
				return res.send({auth: "failed"})
			}else{
				//tells server to proceed to processing request
				next();
			}
		})
	}
}

//token decoding
module.exports.decode = (token) => {
	if(typeof token !== "undefined") {//token is present
		token = token.slice(7, token.length)
		return jwt.verify(token, secret, (err, data) => {
			if (err) {
				console.log(err)
				return null
			}else{
				return jwt.decode(token, {complete: true}).payload
				//payload - decides the data to be decoded, w/c is the token
				//payload - is the one that we need to verify the user info. this is part of token when we code the createAaccessToken function (the one w _id, email & isAdmin)
			}
		})
	}else{
		return null //there is no token
	}
}