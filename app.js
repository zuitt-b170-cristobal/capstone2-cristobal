// setting up dependencies
const express = require("express")
const mongoose = require("mongoose")
const cors = require("cors")

// access to routes
const userRoutes = require("./routes/userRoutes")
const productRoutes = require("./routes/productRoutes")
const orderRoutes = require("./routes/orderRoutes")


// server
const app = express()
const  port = 4000

app.use(cors())//allows all origins/domains to access the backend application

app.use(express.json())
app.use(express.urlencoded( { extended:true } ))




mongoose.connect("mongodb+srv://clarissacristobal:clarissacristobal@wdc028-course-booking.s6ug6.mongodb.net/Capstone2-Ecommerce?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology:true
})

let db = mongoose.connection;
// if an error existed in connecting to MongoDB
db.on("error", console.error.bind(console, "Connection Error"));
// if the connection is successful
db.once("open", () => console.log("We're connected to the database"))


// defines the routes where the CRUD operations will be executed on the users ("/users"), products ("/products") and orders ("/orders")
app.use("/cap/users", userRoutes);
app.use("/cap/products", productRoutes);
app.use("/cap/orders", orderRoutes);


app.listen(port, () => console.log(`API now online at port ${port}`))

//app.listen(process.env.PORT || port, () => console.log(`API now online at port ${process.env.PORT || port}`))

//process.env.PORT works if you are deploying the api in a host like Heroku. This code allows the app to use the environment of the host(Heroku) in running the server
//https://protected-brushlands-54138.herokuapp.com/ | https://git.heroku.com/protected-brushlands-54138.git